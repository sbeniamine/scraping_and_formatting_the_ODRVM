This repository hosts the code used to scrape and format the [Oxford Online Database of Romance Verb Morphology](http://romverbmorph.clp.ox.ac.uk/) into a [CLDF](https://cldf.clld.org/) WordList module. The result can be found as the [Romance Verbal Inflection Dataset](https://gitlab.com/sbeniamine/Romance_Verbal_Inflection_Dataset). This is not the data itself, but the code used to produce it, for reproductibility purposes.

# Dependencies

- pandas
- pycldf
- geopandas
- shapely
- pdcldf 
- unidecode 
- scrapy
- jupyter

# Usage

If all you need is the data, please head to the [Romance Verbal Inflection Dataset](https://gitlab.com/sbeniamine/Romance_Verbal_Inflection_Dataset) repo. If you want to recreate the data yourself, the steps are the following:

First, Run the scrapy spider:

    scrapy crawl --loglevel WARNING ormdb

This can take a long time and should generate two date stamped files, ending in `_lang_index.json` and `paradigm_forms.json`. 

Then, launch a jupyter notebook server:

    jupyter notebook

and open the ` ODRVM_to_CLDF.ipynb` notebook. Play each cell in order to generate the data found in the repo [Romance Verbal Inflection Dataset](https://gitlab.com/sbeniamine/Romance_Verbal_Inflection_Dataset).