#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import scrapy


def extract_css(r, query):
    return r.css(query).get(default='').strip()

def extract_xpath(r, query):
    return r.xpath(query).extract()

def clean(text):
    return text.strip().replace("\n", "; ")

class OxfordRomanceMorphologySpider(scrapy.Spider):
    name = 'ormdb'
    start_urls = ['http://romverbmorph.clp.ox.ac.uk/varieties/']

    def parse(self, response):
        print('parsing')
        for href in response.css('.variety a::attr(href)'):
            yield response.follow(href, self.parse_variety)


    def parse_variety(self, response):
        infos = {} # ("name", extract_with_css(response, 'h1::text') )

        # Get basic info
        # First list right after the Name, Place and Date title
        infos_xpath = "//h2[contains(.,'Name, Place and Date')]/following-sibling::ul[position()=1]/li/text()"
        infos.update(self.parse_list(response, infos_xpath))

        # Get references
        # p in the first div after the References title
        reference_xpath = "//h2[contains(., 'References')]/following-sibling::div[position()=1]/p/text()"
        references = "".join([clean(c) for c in extract_xpath(response, reference_xpath)])
        infos['References'] = references

        # Get notes
        # No need for xpath because of a reliable class
        infos.update(self.parse_note(response, '#content > ul.semantic_list li'))

        yield {"type": "variety", "content": infos}

        for href in response.css('#content > ul#verbs li a::attr(href)'):
            yield response.follow(href, callback=self.parse_paradigm, meta={"variety": infos["Dialect"]})

    def parse_list(self, r, selector):
        for item in extract_xpath(r, selector):
            key, value = item.strip().split(":", 1)
            yield key.strip(), clean(value)

    def parse_note(self, r, selector):
        notes = r.css(selector)
        general_notes = []
        for note in notes:
            subject = note.css('h3::text').get()
            note_content = note.css('p::text').get()
            if subject:
                note_name = "Note on " + subject.strip()
                yield note_name, clean(note_content)
            elif note_content:
                general_notes.append(clean(note_content))
        if general_notes:
            content = "; ".join(general_notes)
            yield "General note", content

    def parse_paradigm(self, response):
        variety = response.meta["variety"]
        infos = {"variety": variety}
        forms = []
        missing_categories = {"No Form", "Not given"}

        # Get basic infos
        infos_xpath = "//h2[contains(.,'Etymology and Meaning')]/following-sibling::ul[position()=1]/li/text()"
        infos.update(self.parse_list(response, infos_xpath))

        # Get comments
        # No need for xpath because of a reliable class
        for name, value in self.parse_note(response, '#content > ul.semantic_list li p'):
            infos[name] = value

        paradigm_xpath = "//h2[contains(.,'Grammatical Categories')]/following-sibling::ul[position()=1]/li"
        for li in response.xpath(paradigm_xpath):
            _, missing_status = li.xpath("span/text()").extract()
            missing_status = missing_status.strip(" \t\n-")
            tense_mood = li.xpath('span/a/text()').get().strip()
            if missing_status in missing_categories:
                forms.append({"verb form":"any", "tense mood":tense_mood, "form":missing_status, **infos})
            else:
                for verb_form in li.css("div.grammatical_function_conjugations div.conjugation_field"):
                    elts = verb_form.css("div.field *::text").getall()
                    elts = list(filter(lambda x:x.strip(), elts))
                    cell, form = ''.join(elts).strip().split(':', 1)
                    forms.append({"verb form":cell.strip(), "tense mood":tense_mood, "form":form.strip(), **infos})

        yield {"type": "paradigm", "content": forms}