# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html


import json
import datetime



class ScrapeormdbPipeline(object):
    def open_spider(self, spider):
        now = datetime.datetime.now().strftime('%Y-%m-%d-%H:%M')
        self.lang_file = open('{}_lang_index.json'.format(now), 'w', encoding='utf8')
        self.paradims_file = open('{}_paradigm_forms.json'.format(now), 'w', encoding='utf8')

    def close_spider(self, spider):
        self.lang_file.close()
        self.paradims_file.close()

    def process_item(self, item, spider):
        if item["type"] == "paradigm":
            out_file = self.paradims_file
        else:
            out_file = self.lang_file
        line = json.dumps(dict(item)) + "\n"
        out_file.write(line)
        return item
